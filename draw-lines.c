#include <stdio.h>
#include <stdlib.h>

struct Line
{
	int X1, Y1, X2, Y2;
};

void eusage(const char *argv0)
{
	fprintf(stderr, "usage: %s segment-list.txt output-image.ppm\n", argv0);
	exit(0);
}

int main(int argc, char *argv[])
{
	char *argv0 = argv[0]; --argc; ++argv;
	char *input_filename = argv[0]; --argc; ++argv;
	char *output_filename = argv[0]; --argc; ++argv;

	if (argc != 0) { eusage(argv0); }

	FILE *input_file = fopen(input_filename, "r");
	if (input_file == NULL)
	{
		perror("fopen");
		exit(1);
	}

	int line_count;
	if (fscanf(input_file, "%d", &line_count) != 1)
	{
		perror("fscanf");
		exit(2);
	}

	struct Line *lines = calloc(line_count, sizeof (struct Line));
	struct Line window;

	for (int i = 0; i < line_count; ++i)
	{
		if (fscanf(input_file, "%d %d %d %d",
			&lines[i].X1, &lines[i].Y1, &lines[i].X2, &lines[i].Y2) != 4)
		{
			perror("fscanf");
			exit(3);
		}
	}

	printf("Read %d lines.\n", line_count);

	if (fscanf(input_file, "%d %d %d %d",
		&window.X1, &window.Y1, &window.X2, &window.Y2) != 4)
	{
		perror("fscanf");
		exit(3);
	}

	free(lines);
	fclose(input_file)	;
	return 0;
}
