#!/bin/env python
import sys
from PIL import Image, ImageDraw

def load_lines_from_file(filename):
	"""
	Read list of lines and a cropping window from file
	"""
	lines = []
	file = open(filename, "r")
	content = file.read().split()
	count = int(content[0])
	for i in range(count + 1):
		x1 = int(content[i * count + 1])
		y1 = int(content[i * count + 2])
		x2 = int(content[i * count + 3])
		y2 = int(content[i * count + 4])
		lines.append((x1,y1,x2,y2))
	return lines[:-1], lines[-1]

def to_real_coords(x1, y1, x2, y2, window):
	wx1, wy1, wx2, wy2 = window
	width = abs(wx2 - wx1)
	height = abs(wy2 - wy1)
	x1 += width/2-wx1
	y1 += height/2-wy1
	x2 += width/2-wx1
	y2 += height/2-wy1
	return x1, y1, x2, y2

if __name__ == "__main__":
	if len(sys.argv) != 2:
		print("usage:", sys.argv[0], "segment-list.txt")
		sys.exit(0)
	lines, window = load_lines_from_file(sys.argv[1])
	print(lines)
	print(window)

	wx1, wy1, wx2, wy2 = window
	window_width = abs(wx2 - wx1)
	window_height = abs(wy2 - wy1)

	img = Image.new(mode = "RGB", size = (window_width * 2, window_height * 2), color = "#234efc")
	d = ImageDraw.Draw(img)
	d.rectangle(to_real_coords(wx1, wy1, wx2, wy2, window), fill = "#cee8ff")
	for line in lines:
		x1, y1, x2, y2 = line
		d.line(to_real_coords(x1, y1, x2, y2, window), fill = "#ffff00")
	img.show()
